# VM Linux pour le CNAM

Cette VM permet de faire les TP. 

## Pre-requis
L'idée est d'avoir une machine Linux. 
Je montre une méthode simple sous Windows

- GitBash https://gitforwindows.org/ (ou https://git-scm.com/down)
- VirtualBox https://www.virtualbox.org/wiki/Downloads
- Vagrant https://www.vagrantup.com/downloads.html


## Créer la box
0. Lancer GitBash, créer ou aller dans votre repertoire projet
1. Sous Windows exclusivement, configurer Git `git config --global core.autocrlf false`
1. Cloner ce repository: `git clone https://...` voir l'adresse en haut à droite du site.
2. Lancer `vagrant init`

## Utiliser la box
0. Lancer `vagrant up` (demarre la machine, c'est long la premiere fois)
1. Lancer `vagrant ssh` (se connecte en SSH à la machine)
2. Travailler ... (il est possible de lancer plusieurs GitBash, de faire `vagrant ssh` pour avoir plusieurs fenetres sur la machine.)
3. Taper `exit` pour sortir de la session SSH
4. Lancer `vagrant halt` pour arreter la box.


